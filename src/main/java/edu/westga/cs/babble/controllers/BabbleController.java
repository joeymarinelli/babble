package edu.westga.cs.babble.controllers;

import edu.westga.cs.babble.model.EmptyTileBagException;
import edu.westga.cs.babble.model.PlayedWord;
import edu.westga.cs.babble.model.Tile;
import edu.westga.cs.babble.model.TileBag;
import edu.westga.cs.babble.model.TileRack;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

/**
 * This is the BabbleController class. It will hold the methods that allow the user to interact with the GUI
 * @author Joey Marinelli
 * @version 8/20/18
 *
 */
public class BabbleController {
	private TileRack rack;
	private PlayedWord word;
	private TileBag bag;
	private WordDictionary dictionary;
	private int totalScore;

	@FXML
	private ListView<Tile> tileListView;
	@FXML
	private ListView<Tile> wordListView;
	@FXML private TextField scoreField;

	/**
	 * BabbleController constructor. Initializes instance variable and populates initial tileRack
	 */
	public BabbleController() {
		this.rack = new TileRack();
		this.word = new PlayedWord();
		this.bag = new TileBag();
		this.dictionary = new WordDictionary();
		this.totalScore = 0;

		this.populateRack();

	}

	/**
	 * initializes the FXML gui elements
	 */
	@FXML
	public void initialize() {
		this.tileListView.setItems(this.rack.tiles());
		this.wordListView.setItems(this.word.tiles());

		this.rackFactory();
		this.wordFactory();

	}

	/**
	 * private helper method to add tiles to the tileRack
	 */
	private void populateRack() {

		try {
			Tile tileDrawn;
			for (int playerTile = this.rack.getNumberOfTilesNeeded(); playerTile > 0; playerTile--) {
				tileDrawn = this.bag.drawTile();
				this.rack.append(tileDrawn);
			}
		} catch (EmptyTileBagException ete) {
			Alert emptyAlert = new Alert(AlertType.WARNING, "Tile bag is empty");
			emptyAlert.show();
		}
	}

	/**
	 * private helper method to handle the user selecting a tile
	 * @throws Exception
	 */
	@FXML
	private void handleTileSelected() throws Exception {
		try {
			Tile selectedTile = this.tileListView.getSelectionModel().getSelectedItem();
			this.tileListView.getSelectionModel().clearAndSelect(0);
			this.rack.remove(selectedTile);
			this.word.append(selectedTile);
			this.rackFactory().setAccessibleText(new BabbleCell().getText());
		} catch (RuntimeException re) {
			Alert reAlert = new Alert(AlertType.WARNING, "There are no tiles here");
			reAlert.show();
		}

	}

	/**
	 * private helper method to allow the user to put a tile back into the rack after it was moved to the word section
	 * @throws Exception
	 */
	@FXML
	private void handleTileUnselected() throws Exception {
		try {
			this.unselectTile();
		} catch (RuntimeException re) {
			Alert reAlert = new Alert(AlertType.WARNING, "There are no tiles here");
			reAlert.show();
		}

	}

	/**
	 * handles the reset button
	 * @throws Exception
	 */
	@FXML
	private void handleResetButton() throws Exception {
		this.reset();
	}

	/**
	 * handles the Play Word button
	 * @throws Exception
	 */
	@FXML
	private void handlePlayWordButton() throws Exception {
		String word = this.word.getHand();

		if (this.dictionary.isValidWord(word)) {
			this.scoreProperty();
			this.wordListView.getItems().clear();
			this.word.clear();
			this.wordFactory().setAccessibleText(word);
			this.populateRack();
		} else {
			this.reset();
			Alert notAWordAlert = new Alert(AlertType.WARNING, word + " is not a word");
			notAWordAlert.show();
		}
		
	}

	/**
	 * Factory method to make tiles in rack appear as letters instead of objects
	 * @return BabbleCell object
	 */
	private ListCell<Tile> rackFactory() {
		this.tileListView.setCellFactory(new Callback<ListView<Tile>, ListCell<Tile>>() {
			@Override
			public ListCell<Tile> call(ListView<Tile> list) {
				return new BabbleCell();
			}
		});
		
		return new BabbleCell();
	}

	/**
	 * Factory method to make tiles in PlayedWord appear as letters instead of objects 
	 * @return BabbleCell object
	 */
	private ListCell<Tile> wordFactory() {
		this.wordListView.setCellFactory(new Callback<ListView<Tile>, ListCell<Tile>>() {
			@Override
			public ListCell<Tile> call(ListView<Tile> list) {
				return new BabbleCell();
			}
		});
		return new BabbleCell();
	}

	/**
	 * private helper method to allow user to move a tile back into the tile rack
	 * @throws Exception
	 */
	private void unselectTile() throws Exception {
		Tile selectedTile = this.wordListView.getSelectionModel().getSelectedItem();
		this.wordListView.getSelectionModel().clearAndSelect(0);
		this.word.remove(selectedTile);
		this.rack.append(selectedTile);
		this.wordFactory().setAccessibleText(new BabbleCell().getText());
	}

	/**
	 * private helper method to allow user to reset the game
	 * @throws Exception
	 */
	private void reset() throws Exception {
		for (int wordLength = this.word.tiles().size(); wordLength > 0; wordLength--) {
			this.wordListView.getSelectionModel().select(0);
			this.unselectTile();
		}
	}
	
	/**
	 * private helper method that increases the players score
	 * @param score
	 * @return
	 */
	private int updateScore(int score) {
		this.totalScore += score;	
		
		return this.totalScore;
	}
	
	/**
	 * private helper method that binds the players score field to an IntegerProperty
	 */
	private void scoreProperty() {
		IntegerProperty scoreProperty = new SimpleIntegerProperty();
		scoreProperty.set(this.updateScore(this.word.getScore()));
		ObservableValue<? extends String> scoreString = scoreProperty.asString();
		this.scoreField.textProperty().bind(scoreString);
	}
}
