package edu.westga.cs.babble.controllers;

import edu.westga.cs.babble.model.Tile;
import javafx.scene.control.ListCell;

/**
 * This is the BabbleCell class, it will update the tiles to appear as a letter. 
 * @author Joey Marinelli
 * @version 8/22/18
 */
public class BabbleCell extends ListCell<Tile> {
	
	@Override
	public void updateItem(Tile item, boolean empty) {
		super.updateItem(item, empty);
		if (item != null) {
			setText(item.getLetter() + "");
		}
	}
	
}
