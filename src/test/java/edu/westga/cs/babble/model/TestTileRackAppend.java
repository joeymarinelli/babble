package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the append method of the TileRack class. 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestTileRackAppend {

	/**
	 * This test will confirm that an exception is thrown when you try to append an eighth tile to the tileRack. 
	 */
	@Test
	void shouldNotAppendToFullRack() {
		assertThrows(TileRackFullException.class, () -> {
			TileRack testGroup = new TileRack();
			Tile tileA = new Tile('A');
			Tile tileB = new Tile('B');
			Tile tileC = new Tile('C');
			Tile tileD = new Tile('D');
			Tile tileE = new Tile('E');
			Tile tileF = new Tile('F');
			Tile tileG = new Tile('G');
			Tile tileH = new Tile('H');
			
			testGroup.append(tileA);
			testGroup.append(tileB);
			testGroup.append(tileC);
			testGroup.append(tileD);
			testGroup.append(tileE);
			testGroup.append(tileF);
			testGroup.append(tileG);
			testGroup.append(tileH);
		});
	}

}
