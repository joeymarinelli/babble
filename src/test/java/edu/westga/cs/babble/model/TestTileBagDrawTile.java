package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * This is
 * 
 * @author Joey
 *
 */
class TestTileBagDrawTile {

	@Test
	void canDrawAllTiles() {
		TileBag testBag = new TileBag();
		for (int draw = 0; draw < 97; draw++) {
			try {
				testBag.drawTile();
			} catch (EmptyTileBagException etbe) {
				etbe.printStackTrace();
			}
		}
		assertEquals(false, testBag.isEmpty());

		try {
			testBag.drawTile();
		} catch (EmptyTileBagException etbe) {
			etbe.printStackTrace();
		}

		assertEquals(true, testBag.isEmpty());
	}

	/**
	 * This test will confirm that an exception is thrown when the drawTile method
	 * is called too many times.
	 */
	@Test
	void canNotDrawTooManyTiles() {
		assertThrows(EmptyTileBagException.class, () -> {
			TileBag testBag = new TileBag();
			for (int draw = 0; draw < 99; draw++) {
				testBag.drawTile();
			}
		});
	}

	/**
	 * This test will confirm that the letters have the correct distribution.
	 */
	@Test
	void hasProperTileDistribution() {
		TileBag testBag = new TileBag();
		ArrayList<Tile> testList = new ArrayList<Tile>();
		Tile testTile;
		for (int tileNumber = 0; tileNumber < 98; tileNumber++) {
			try {
				testTile = testBag.drawTile();
				testList.add(testTile);
			} catch (EmptyTileBagException e) {
				e.printStackTrace();
			}
		}

		int aCounter = 0;
		int bCounter = 0;
		int cCounter = 0;
		int dCounter = 0;
		int eCounter = 0;
		int fCounter = 0;
		int gCounter = 0;
		int hCounter = 0;
		int iCounter = 0;
		int jCounter = 0;
		int kCounter = 0;
		int lCounter = 0;
		int mCounter = 0;
		int nCounter = 0;
		int oCounter = 0;
		int pCounter = 0;
		int qCounter = 0;
		int rCounter = 0;
		int sCounter = 0;
		int tCounter = 0;
		int uCounter = 0;
		int vCounter = 0;
		int wCounter = 0;
		int xCounter = 0;
		int yCounter = 0;
		int zCounter = 0;
		for (Tile current : testList) {
			switch (current.getLetter()) {
			case 'A':
				aCounter++;
				break;
			case 'B':
				bCounter++;
				break;
			case 'C':
				cCounter++;
				break;
			case 'D':
				dCounter++;
				break;
			case 'E':
				eCounter++;
				break;
			case 'F':
				fCounter++;
				break;
			case 'G':
				gCounter++;
				break;
			case 'H':
				hCounter++;
				break;
			case 'I':
				iCounter++;
				break;
			case 'J':
				jCounter++;
				break;
			case 'K':
				kCounter++;
				break;
			case 'L':
				lCounter++;
				break;
			case 'M':
				mCounter++;
				break;
			case 'N':
				nCounter++;
				break;
			case 'O':
				oCounter++;
				break;
			case 'P':
				pCounter++;
				break;
			case 'Q':
				qCounter++;
				break;
			case 'R':
				rCounter++;
				break;
			case 'S':
				sCounter++;
				break;
			case 'T':
				tCounter++;
				break;
			case 'U':
				uCounter++;
				break;
			case 'V':
				vCounter++;
				break;
			case 'W':
				wCounter++;
				break;
			case 'X':
				xCounter++;
				break;
			case 'Y':
				yCounter++;
				break;
			default:
				zCounter++;
			}
		}

		assertEquals(9, aCounter);
		assertEquals(2, bCounter);
		assertEquals(2, cCounter);
		assertEquals(4, dCounter);
		assertEquals(12, eCounter);
		assertEquals(2, fCounter);
		assertEquals(3, gCounter);
		assertEquals(2, hCounter);
		assertEquals(9, iCounter);
		assertEquals(1, jCounter);
		assertEquals(1, kCounter);
		assertEquals(4, lCounter);
		assertEquals(2, mCounter);
		assertEquals(6, nCounter);
		assertEquals(8, oCounter);
		assertEquals(2, pCounter);
		assertEquals(1, qCounter);
		assertEquals(6, rCounter);
		assertEquals(4, sCounter);
		assertEquals(6, tCounter);
		assertEquals(4, uCounter);
		assertEquals(2, vCounter);
		assertEquals(2, wCounter);
		assertEquals(1, xCounter);
		assertEquals(2, yCounter);
		assertEquals(1, zCounter);
	}

}
