package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the clear method of the PlayedWord class. 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestPlayedWordClear {

	/**
	 * This test will confirm that an empty word can be cleared
	 */
	@Test
	void shouldClearEmptyWord() {
		PlayedWord testWord = new PlayedWord();
		testWord.clear();
		assertEquals(true, testWord.tiles().isEmpty());
	}
	
	/**
	 * This test will confirm that a word with one tile can be cleared 
	 */
	@Test
	void shouldClearWordWithOneTile() {
		PlayedWord testWord = new PlayedWord();
		Tile tileA = new Tile('A');
		testWord.append(tileA);
		assertEquals(false, testWord.tiles().isEmpty());
		testWord.clear();
		assertEquals(true, testWord.tiles().isEmpty());
	}
	
	/**
	 * This test will confirm that a word with multiple tiles can be cleared. 
	 */
	@Test
	void shouldClearWordWithManyTiles() {
		PlayedWord testWord = new PlayedWord();
		Tile tileA = new Tile('A');
		Tile tileP = new Tile('P');
		Tile tileP2 = new Tile('P');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileA);
		testWord.append(tileP);
		testWord.append(tileP2);
		testWord.append(tileL);
		testWord.append(tileE);
		assertEquals(5, testWord.tiles().size());
		testWord.clear();
		assertEquals(true, testWord.tiles().isEmpty());
	}
}
