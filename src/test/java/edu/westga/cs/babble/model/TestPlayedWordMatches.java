package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the matches method of the PlayedWord class. 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestPlayedWordMatches {

	/**
	 * This test will confirm that matches works properly for a multiple tile word.
	 */
	@Test
	void hasTilesForAMultipleLetterWord() {
		PlayedWord testWord = new PlayedWord();
		Tile tileT = new Tile('T');
		Tile tileI = new Tile('I');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileT);
		testWord.append(tileI);
		testWord.append(tileL);
		testWord.append(tileE);
		
		assertEquals(true, testWord.matches("TILE"));
	}

	/**
	 * This test will confirm that matches works properly for a single letter word. 
	 */
	@Test
	void hasTilesForASingleLetterWord() {
		PlayedWord testWord = new PlayedWord();
		Tile tileT = new Tile('T');
		testWord.append(tileT);
		
		assertEquals(true, testWord.matches("T"));
	}
	
	/**
	 * This test will confirm that the matches method will return false if the tiles are out of order. 
	 */
	@Test
	void cannotMatchWordWhenTilesAreScrambled() {
		PlayedWord testWord = new PlayedWord();
		Tile tileT = new Tile('T');
		Tile tileI = new Tile('I');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileT);
		testWord.append(tileL);
		testWord.append(tileE);
		testWord.append(tileI);
			
		assertEquals(false, testWord.matches("TILE"));
	}
	
	/**
	 * This test will confirm that matches will return false if tiles are insufficient
	 */
	@Test 
	void cannotMatchWordIfInsufficientTiles() {
		PlayedWord testWord = new PlayedWord();
		Tile tileT = new Tile('T');
		Tile tileI = new Tile('I');
		Tile tileL = new Tile('L');
		testWord.append(tileT);
		testWord.append(tileI);
		testWord.append(tileL);
					
		assertEquals(false, testWord.matches("TILE"));
	}
	
	/**
	 * This test will confirm that matches will return true if a word contains duplicate letters. 
	 */
	@Test
	void canMatchWordWithDuplicateLetters() {
		PlayedWord testWord = new PlayedWord();
		Tile tileA = new Tile('A');
		Tile tileP = new Tile('P');
		Tile tileP2 = new Tile('P');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileA);
		testWord.append(tileP);
		testWord.append(tileP2);
		testWord.append(tileL);
		testWord.append(tileE);
			
		assertEquals(true, testWord.matches("APPLE"));
	}
	
	/**
	 * This test will confirm that a nonempty word does not match an empty text
	 */
	@Test
	void nonEmptyWordShouldNotMatchEmptyText() {
		PlayedWord testWord = new PlayedWord();
		Tile tileA = new Tile('A');
		Tile tileP = new Tile('P');
		Tile tileP2 = new Tile('P');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileA);
		testWord.append(tileP);
		testWord.append(tileP2);
		testWord.append(tileL);
		testWord.append(tileE);
			
		assertEquals(false, testWord.matches(""));
	}
	
	/**
	 * This test will confirm that an empty word will not match an empty text.
	 */
	@Test
	void emptyWordShouldNotMatchEmptyText() {
		PlayedWord testWord = new PlayedWord();
		assertEquals(false, testWord.matches(""));
	}
	
	/**
	 * This test will confirm that matched will not accept null argument
	 */
	@Test
	void shouldNotAllowNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			PlayedWord testWord = new PlayedWord();
			testWord.matches(null);
		});
	}
}
