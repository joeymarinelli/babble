package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the remove method of the TileGroup abstract class. 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestTileGroupRemove {

	/**
	 * This test will confirm that you cannot remove a tile from an empty tileGroup
	 */
	@Test
	void canNotRemoveFromEmptyTileGroup() {
		assertThrows(TileNotInGroupException.class, () -> {
			TileGroup testGroup = new TileRack();
			Tile testTile = new Tile('A');
			testGroup.remove(testTile);
		});
		
	}
	
	/**
	 * This test will confirm that you cannot remove a null tile from a tileGroup
	 */
	@Test
	void shouldNotAllowNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			TileGroup testGroup = new TileRack();
			testGroup.remove(null);
		});
	}
	/**
	 * This test will confirm that you cannot remove a tile that is not present in the tileGroup.
	 */
	@Test
	void canNotRemoveTileNotInTileGroup() {
		assertThrows(TileNotInGroupException.class, () -> {
			TileGroup testGroup = new TileRack();
			Tile testTile = new Tile('A');
			testGroup.append(testTile);
			Tile testTile2 = new Tile('B');
			testGroup.remove(testTile2);
		});
	}

	/**
	 * This test will confirm that you can remove the only tile in the tileGroup
	 */
	@Test
	void canRemoveOnlyTileInTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile testTile = new Tile('A');
		testGroup.append(testTile);
		assertEquals("A", testGroup.getHand());
		try {
			testGroup.remove(testTile);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("", testGroup.getHand());
	}
	
	/**
	 * This test will confirm that you can remove the first of multiple tiles in the tileGroup
	 */
	@Test
	void canRemoveFirstOfManyTilesFromTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		testGroup.append(tileA);
		testGroup.append(tileB);
		testGroup.append(tileC);
		testGroup.append(tileD);
		assertEquals("ABCD", testGroup.getHand());
		Tile testTile = testGroup.tiles().get(0);
		try {
			testGroup.remove(testTile);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("BCD", testGroup.getHand());
	}
	
	/**
	 * This test will confirm that you can remove the last of multiple tiles from the tileGroup
	 */
	@Test
	void canRemoveLastOfManyTilesFromTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		testGroup.append(tileA);
		testGroup.append(tileB);
		testGroup.append(tileC);
		testGroup.append(tileD);
		assertEquals("ABCD", testGroup.getHand());
		Tile testTile = testGroup.tiles().get(testGroup.tiles().size() - 1);
		try {
			testGroup.remove(testTile);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("ABC", testGroup.getHand());
	}
	
	/**
	 * This test will confirm that you can remove the middle of multipleTiles from the tileGroup
	 */
	@Test
	void canRemoveMiddleOfManyTilesFromTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		Tile tileE = new Tile('E');
		Tile tileF = new Tile('F');
		Tile tileG = new Tile('G');
		testGroup.append(tileA);
		testGroup.append(tileB);
		testGroup.append(tileC);
		testGroup.append(tileD);
		testGroup.append(tileE);
		testGroup.append(tileF);
		testGroup.append(tileG);
		assertEquals("ABCDEFG", testGroup.getHand());
		Tile testTile = testGroup.tiles().get((testGroup.tiles().size() - 1) / 2);
		try {
			testGroup.remove(testTile);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("ABCEFG", testGroup.getHand());
	}
	
	/**
	 * This test will confirm that you can remove multiple tiles from a tileGroup
	 */
	@Test
	void canRemoveMultipleTilesFromTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		Tile tileE = new Tile('E');
		Tile tileF = new Tile('F');
		Tile tileG = new Tile('G');
		testGroup.append(tileA);
		testGroup.append(tileB);
		testGroup.append(tileC);
		testGroup.append(tileD);
		testGroup.append(tileE);
		testGroup.append(tileF);
		testGroup.append(tileG);
		assertEquals("ABCDEFG", testGroup.getHand());
		Tile testTile = testGroup.tiles().get((testGroup.tiles().size() - 1) / 2);
		try {
			testGroup.remove(testTile);
			testGroup.remove(tileA);
			testGroup.remove(tileG);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("BCEF", testGroup.getHand());
	}
	
	/**
	 * This test will confirm that duplicate tiles are not removed from the tileGroup
	 */
	@Test
	void doesNotRemoveDuplicateTilesFromTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile tile1 = new Tile('D');
		Tile tile2 = new Tile('D');
		Tile tile3 = new Tile('D');
		Tile tile4 = new Tile('D');
		Tile tile5 = new Tile('D');
		Tile tile6 = new Tile('D');
		Tile tile7 = new Tile('D');
		testGroup.append(tile1);
		testGroup.append(tile2);
		testGroup.append(tile3);
		testGroup.append(tile4);
		testGroup.append(tile5);
		testGroup.append(tile6);
		testGroup.append(tile7);
		assertEquals("DDDDDDD", testGroup.getHand());
		try {
			testGroup.remove(tile1);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("DDDDDD", testGroup.getHand());
	}
}
