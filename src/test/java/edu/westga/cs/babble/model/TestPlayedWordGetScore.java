package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the getScore method of the PlayedWord class. 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestPlayedWordGetScore {

	/**
	 * This test will confirm that an empty word has a score of 0
	 */
	@Test
	void emptyWordShouldHaveScoreOfZero() {
		PlayedWord testWord = new PlayedWord();
		assertEquals(0, testWord.getScore());
	}
	
	/**
	 * This test will confirm that a one tile word will be scored
	 */
	@Test
	void scoreAOneTileWord() {
		PlayedWord testWord = new PlayedWord();
		Tile tileA = new Tile('A');
		testWord.append(tileA);
		assertEquals(1, testWord.getScore());
	}
	
	/**
	 * This test will confirm that a multiple tile word will be scored properly
	 */
	@Test
	void scoreAWordWithMultipleDifferingTiles() {
		PlayedWord testWord = new PlayedWord();
		Tile tileT = new Tile('T');
		Tile tileI = new Tile('I');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileT);
		testWord.append(tileI);
		testWord.append(tileL);
		testWord.append(tileE);
		
		assertEquals(4, testWord.getScore());
	}
	
	/**
	 * This test will confirm that a word with duplicate tiles will be scored properly
	 */
	@Test
	void scoreAWordContainingDuplicateTiles() {
		PlayedWord testWord = new PlayedWord();
		Tile tileA = new Tile('A');
		Tile tileP = new Tile('P');
		Tile tileP2 = new Tile('P');
		Tile tileL = new Tile('L');
		Tile tileE = new Tile('E');
		testWord.append(tileA);
		testWord.append(tileP);
		testWord.append(tileP2);
		testWord.append(tileL);
		testWord.append(tileE);
		
		assertEquals(9, testWord.getScore());
	}

}
