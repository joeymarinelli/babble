package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the TileGroupAppend method in the TileGroup class.
 * 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestTileGroupAppend {

	/**
	 * This test will confirm that the append method in the tileGroup class does not
	 * allow a null argument.
	 */
	@Test
	void shouldNotAllowNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			TileGroup testGroup = new TileRack();
			testGroup.append(null);
		});
	}

	/**
	 * This test will confirm that an empty group has an empty string.
	 */
	@Test
	void emptyGroupShouldBeEmpty() {
		TileGroup testGroup = new PlayedWord();
		String testHand = testGroup.getHand();
		assertEquals("", testHand);
	}

	/**
	 * This test will confirm that the TileGroup has only one tile added to it.
	 */
	@Test
	void shouldHaveOneTileInTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile testTile = new Tile('A');
		testGroup.append(testTile);

		assertEquals("A", testGroup.getHand());
	}

	/**
	 * This test will confirm that you can add multiple tiles to the tileGroup
	 */
	@Test
	void shouldHaveManyTilesInTileGroup() {
		TileGroup testGroup = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		
		testGroup.append(tileA);
		testGroup.append(tileB);
		testGroup.append(tileC);
		testGroup.append(tileD);
		
		assertEquals("ABCD", testGroup.getHand());
	}

	/**
	 * This test will confirm that you can add duplicate letters to the tileGroup
	 */
	 @Test void shouldHaveManyTilesIncludingDuplicatesInTileGroup() {
			TileGroup testGroup = new TileRack();
			Tile tileA = new Tile('A');
			Tile tileB = new Tile('B');
			Tile tileC = new Tile('C');
			Tile tileD = new Tile('D');
			Tile tileA2 = new Tile('A');
			Tile tileB2 = new Tile('B');
			Tile tileC2 = new Tile('C');
			
			testGroup.append(tileA);
			testGroup.append(tileB);
			testGroup.append(tileC);
			testGroup.append(tileD);
			testGroup.append(tileA2);
			testGroup.append(tileB2);
			testGroup.append(tileC2);
			
			assertEquals("ABCDABC", testGroup.getHand());
		}
	 
	  /**
	   * This test will confirm that an IllegalArgumentException is thrown if you try to add the same tile more than once. 
	   */
	 @Test void cannotAddSameTileTwice() {
		 assertThrows(IllegalArgumentException.class, () -> {
				TileGroup testGroup = new TileRack();
				Tile testTile = new Tile('A');
				testGroup.append(testTile);
				testGroup.append(testTile);
			});
	 }
	 
}
