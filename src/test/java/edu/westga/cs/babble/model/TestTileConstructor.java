package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * This class will test the constructor of the Tile class. 
 * @author Joey Marinelli
 * @version 8/23/18
 *
 */
class TestTileConstructor {
	
	/**
	 * This test will confirm that the constructor does not allow nonletters. 
	 */
	@Test
	void shouldNotAllowNonLetters() {

		assertThrows(IllegalArgumentException.class, () -> {
			Tile testTile = new Tile(' ');
			testTile.getLetter();
		});
		
		assertThrows(IllegalArgumentException.class, () -> {
			Tile testTile = new Tile('1');
			testTile.getLetter();
		});
		
		assertThrows(IllegalArgumentException.class, () -> {
			Tile testTile = new Tile(';');
			testTile.getLetter();
		});
	}
	
	/**
	 * This test will confirm that all of the one-point tiles have a vaue of one. 
	 */
	@Test
	void shouldCreateOnePointTiles() {
		Tile testA = new Tile('A');
		Tile testa = new Tile('a');
		Tile testE = new Tile('E');
		Tile teste = new Tile('e');
		Tile testI = new Tile('I');
		Tile testi = new Tile('i');
		Tile testO = new Tile('O');
		Tile testo = new Tile('o');
		Tile testN = new Tile('N');
		Tile testn = new Tile('n');
		Tile testR = new Tile('R');
		Tile testr = new Tile('r');
		Tile testT = new Tile('T');
		Tile testt = new Tile('t');
		Tile testL = new Tile('L');
		Tile testl = new Tile('l');
		Tile testS = new Tile('S');
		Tile tests = new Tile('s');
		Tile testU = new Tile('U');
		Tile testu = new Tile('u');
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		listOfTiles.add(testA);
		listOfTiles.add(testa);
		listOfTiles.add(testE);
		listOfTiles.add(teste);
		listOfTiles.add(testI);
		listOfTiles.add(testi);
		listOfTiles.add(testO);
		listOfTiles.add(testo);
		listOfTiles.add(testN);
		listOfTiles.add(testn);
		listOfTiles.add(testR);
		listOfTiles.add(testr);
		listOfTiles.add(testT);
		listOfTiles.add(testt);
		listOfTiles.add(testL);
		listOfTiles.add(testl);
		listOfTiles.add(testS);
		listOfTiles.add(tests);
		listOfTiles.add(testU);
		listOfTiles.add(testu);
		
		boolean equalsOne = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 1) {
				equalsOne = false;
			}
		}
		
		assertEquals(true, equalsOne);
	}
	
	/**
	 * This test will confirm that all of the two-point tiles have a vaue of two. 
	 */
	@Test
	void shouldCreateTwoPointTiles() {
		Tile testD = new Tile('D');
		Tile testd = new Tile('d');
		Tile testG = new Tile('G');
		Tile testg = new Tile('g');
		
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		
		listOfTiles.add(testD);
		listOfTiles.add(testd);
		listOfTiles.add(testG);
		listOfTiles.add(testg);
		
		boolean equalsTwo = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 2) {
				equalsTwo = false;
			}
		}
		
		assertEquals(true, equalsTwo);
	}
	
	/**
	 * This test confirms that all of the three point tiles have a value of three
	 */
	@Test
	void shouldCreateThreePointTiles() {
		Tile testB = new Tile('B');
		Tile testb = new Tile('b');
		Tile testC = new Tile('C');
		Tile testc = new Tile('c');
		Tile testM = new Tile('M');
		Tile testm = new Tile('m');
		Tile testP = new Tile('P');
		Tile testp = new Tile('p');
		
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		
		listOfTiles.add(testB);
		listOfTiles.add(testb);
		listOfTiles.add(testC);
		listOfTiles.add(testc);
		listOfTiles.add(testM);
		listOfTiles.add(testm);
		listOfTiles.add(testP);
		listOfTiles.add(testp);
		
		boolean equalsThree = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 3) {
				equalsThree = false;
			}
		}
		
		assertEquals(true, equalsThree);
	}
	
	/**
	 * This test confirms that all of the four point tiles have a value of four
	 */
	@Test
	void shouldCreateFourPointTiles() {
		Tile testF = new Tile('F');
		Tile testf = new Tile('f');
		Tile testH = new Tile('H');
		Tile testh = new Tile('h');
		Tile testV = new Tile('V');
		Tile testv = new Tile('v');
		Tile testW = new Tile('W');
		Tile testw = new Tile('w');
		Tile testY = new Tile('Y');
		Tile testy = new Tile('y');
		
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		
		listOfTiles.add(testF);
		listOfTiles.add(testf);
		listOfTiles.add(testH);
		listOfTiles.add(testh);
		listOfTiles.add(testV);
		listOfTiles.add(testv);
		listOfTiles.add(testW);
		listOfTiles.add(testw);
		listOfTiles.add(testY);
		listOfTiles.add(testy);
		boolean equalsFour = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 4) {
				equalsFour = false;
			}
		}
		
		assertEquals(true, equalsFour);
	}
	
	/**
	 * This test will confirm that the five point tiles have a value of five
	 */
	@Test
	void shouldCreateFivePointTiles() {
		Tile testK = new Tile('K');
		Tile testk = new Tile('k');
		
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		
		listOfTiles.add(testK);
		listOfTiles.add(testk);
		
		boolean equalsFive = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 5) {
				equalsFive = false;
			}
		}
		
		assertEquals(true, equalsFive);
	}
	
	/**
	 * This test will confirm that eight point tiles have value eight
	 */
	@Test
	void shouldCreateEightPointTiles() {
		Tile testJ = new Tile('J');
		Tile testj = new Tile('j');
		Tile testX = new Tile('X');
		Tile testx = new Tile('x');
		
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		
		listOfTiles.add(testJ);
		listOfTiles.add(testj);
		listOfTiles.add(testX);
		listOfTiles.add(testx);
		
		boolean equalsEight = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 8) {
				equalsEight = false;
			}
		}
		
		assertEquals(true, equalsEight);
	}
	
	/**
	 * This test will confirm that the ten point tiles have a value of ten 
	 */
	@Test
	void shouldCreateTenPointTiles() {
		Tile testQ = new Tile('Q');
		Tile testq = new Tile('q');
		Tile testZ = new Tile('Z');
		Tile testz = new Tile('z');
		
		ArrayList<Tile> listOfTiles = new ArrayList<Tile>();
		
		listOfTiles.add(testQ);
		listOfTiles.add(testq);
		listOfTiles.add(testZ);
		listOfTiles.add(testz);
		
		boolean equalsTen = true;
		
		for(Tile current : listOfTiles) {
			if (current.getPointValue() != 10) {
				equalsTen = false;
			}
		}
		
		assertEquals(true, equalsTen);
	}

}
