package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * This is the test class for the getNumberOfTilesNeeded method of the tileRack class. 
 * @author Joey Marinelli
 * @version 8/25/18
 *
 */
class TestTileRackGetNumberOfTilesNeeded {

	/**
	 * This test will confirm that an empty rack has the max number of tiles needed. 
	 */
	@Test
	void emptyTileRackShouldNeedMaxSizeNumberOfTiles() {
		TileRack testRack = new TileRack();
		assertEquals(TileRack.MAX_SIZE, testRack.getNumberOfTilesNeeded());
	}
	
	/**
	 * This test will confirm that a rack with one tile will need one less than the max number of tiles. 
	 */
	@Test
	void tileRackWithOneTileShouldNeedMaxSizeMinusOneTiles() {
		TileRack testRack = new TileRack();
		Tile testTile = new Tile('A');
		testRack.append(testTile);
		assertEquals(TileRack.MAX_SIZE - 1, testRack.getNumberOfTilesNeeded());
	}
	
	/**
	 * This test will confirm that a rack with several tiles will need the max size minus the number of tiles in the rack. 
	 */
	@Test
	void tileRackWithSeveralTilesShouldNeedSomeTiles() {
		TileRack testRack = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		testRack.append(tileA);
		testRack.append(tileB);
		testRack.append(tileC);
		testRack.append(tileD);
		assertEquals(TileRack.MAX_SIZE - 4, testRack.getNumberOfTilesNeeded());
	}

	/**
	 * This test will confirm that a full rack will need 0 tiles. 
	 */
	@Test
	void fullRackNeedsZeroTiles() {
		TileRack testRack = new TileRack();
		Tile tileA = new Tile('A');
		Tile tileB = new Tile('B');
		Tile tileC = new Tile('C');
		Tile tileD = new Tile('D');
		Tile tileE = new Tile('E');
		Tile tileF = new Tile('F');
		Tile tileG = new Tile('G');
		testRack.append(tileA);
		testRack.append(tileB);
		testRack.append(tileC);
		testRack.append(tileD);
		testRack.append(tileE);
		testRack.append(tileF);
		testRack.append(tileG);
		assertEquals(0, testRack.getNumberOfTilesNeeded());
	}
}
